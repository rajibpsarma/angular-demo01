# Angular demo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

It shows various concepts of Angular.

## The steps to be followed are: ##
* git clone https://rajibpsarma@bitbucket.org/rajibpsarma/angular-demo01.git
* ng new angular-demo01
* cd angular-demo01
* npm install bootstrap --save
* ng serve
* Now explore the app using "http://localhost:4200/". It outputs various things to JS console.