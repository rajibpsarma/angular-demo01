import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  private route:ActivatedRoute;
  server : {id: number, name:string};
  constructor(route:ActivatedRoute) {
    this.route = route;
  }

  ngOnInit(): void {
    let id = this.route.snapshot.params['id'];
    this.server = {
      id: id,
      name: "Server " + id
    }
  }

}
