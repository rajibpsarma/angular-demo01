import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReceptionComponent } from './reception/reception.component';
import { HomeComponent } from './home/home.component';
import { ServersComponent } from './servers/servers.component';
import { ServerComponent } from './servers/server/server.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { Parent1Component } from './parent/parent1.component';
import { Child1Component } from './child/child1.component';

const appRoutes:Routes = [
  {path:'', component:HomeComponent}, // When the path is navigated, this component is loaded
  {path:'servers', component:ServersComponent},
  {path:'servers/:id', component:ServerComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ReceptionComponent,
    HomeComponent,
    ServersComponent,
    ServerComponent,
    ParentComponent,
    ChildComponent,
    Parent1Component,
    Child1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	  FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
