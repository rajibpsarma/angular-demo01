import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

/*
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
*/

function echo<T>(data : T) : T {
 return data;
}
/*
interface GenericI {
  m1(data:number):number;
  <T>(data:T):T;
}
*/
/*
class GenericNumber {
  add(no1: T, no2 : T):T {

  }
}
*/
export const enum Direction {
  North,  //0
  South,  //1
  East,   //2
  West    //3
}

@Component({
  selector: 'welcome-guest',
  templateUrl: './app.component.html',
  styles:['']
})
export class AppComponent implements OnInit {
  guestNames = ["Tom", "Dick", "Harry"];

  constructor(private router : Router){}

  ngOnInit() {
    let val = echo(2);
    console.log(val + ":" + typeof val);    // 2:number

    let val1 = echo("abc");
    console.log(val1 + ":" + typeof val1);    // abc:string

    let val2 = echo({name:"rajib"});
    console.log(JSON.stringify(val2) + ":" + typeof val2);    // {"name":"rajib"}:object

    let val3 = [1,2,3];
    console.log(val3 + ":" + typeof val3);    // 1,2,3:object
    {
      /*
      let name:string;
      console.log(name.charAt(0));
      */
    }

    /*
    this.router.events.subscribe(
      (e) => {
        console.log("Routing Event : " + e);
        if(e instanceof NavigationStart) {
          console.log("Navigation Started for path : " + e.url); // "/servers"
        }
      }
    };
      */
/*
      let arr1 = [1,2,3];
      let arr2 = [7,8,9];

      let arrNew1 = [...arr1]; // Same as arr1
      console.log(arrNew1);
      let arrNew2 = [...arr1, 4, 5, 6]; // 1,2,3,4,5,6
      console.log(arrNew2);
      let arrNew3 = [...arrNew2, ...arr2]; // 1,2,3,4,5,6,7,8,9
      console.log(arrNew3);
*/
/*
    let obj1 = {name:"rajib"};
    let obj2 = {...obj1, age:"100"}; // name, age
    console.log(obj2);
    let obj3 = {sex : "male"};
    let obj4 = {...obj2, ...obj3}; // name, age, sex
    console.log(obj4);
*/
    //console.log("North", Direction.North);
/*
    let map = {name:"rajib", age:"100", address:"blore"};
    for(let key in map) {
      console.log(key + ", " + map[key]);
    }
    */
/*
   let map = new Map();
   map.set("name","rajib");
   map.set("age","100");
   console.log(map)


   let set = new Set();
   set.add("Rajib");
   set.add("Sanjib");
   set.add("Chiran");
   console.log("Length : " + set.size); // 3
   console.log("Has Rajib : " + set.has("Rajib")); // true
   // Iterate
   set.forEach((item, index) => {
     console.log(item)
   })
   set.clear();
   console.log("Length : " + set.size); // 0
   */
  }
/*
  doThis() {
    let name = "rajib";
    {
      let name = "abc";
    }
  }
  */
}
