import { Component, OnInit, ViewChild, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: "./parent.component.html"
})
export class ParentComponent implements OnInit, AfterViewInit {

  @ViewChild(ChildComponent) child : ChildComponent;
  @ViewChildren(ChildComponent) children : QueryList<ChildComponent>;
  constructor() { }

  ngOnInit(): void {
    //console.log("Child Name : " + this.child.getComponentName());
  }

  ngAfterViewInit() {
    //console.log("Child Name : " + this.child.getComponentName());
  }

  getChildName() {
    console.log("Child Name : " + this.child.getComponentName());

    // Get each child names
    this.children.forEach(
      (item:ChildComponent, index:number) => {
        console.log("Child #" + (index+1) + " Name : " + item.getComponentName());
      }
    );
  }
}
