import { Component, ViewChild, ContentChild } from '@angular/core';
import { Child1Component } from '../child/child1.component';

@Component({
  selector : "app-parent1",
  template : "<ng-content></ng-content><button (click)='getContent()'>Get</button>"
})
export class Parent1Component {
  @ContentChild("myinput") child;
  constructor() { }

  ngOnInit(): void {
    //console.log("Child Name : " + this.child.getComponentName());
  }

  ngAfterViewInit() {
    //console.log("Child Name : " + this.child.getComponentName());
  }

  getContent() {
    console.log("Content value : " + this.child.nativeElement.value);
/*
    // Get each child names
    this.children.forEach(
      (item:ChildComponent, index:number) => {
        console.log("Child #" + (index+1) + " Name : " + item.getComponentName());
      }
    );
    */
  }
}
