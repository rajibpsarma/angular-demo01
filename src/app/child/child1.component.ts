import { Component, Input } from '@angular/core';


@Component({
  template:"<div>Child {{childName}} </div><ng-content>",
  selector:"app-child1"
})
export class Child1Component {
  @Input() childName;

  getComponentName() {
    //return this.childName;
    return "abc";
  }
}
