import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit, OnChanges {

  @Input() childName : string;
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes : SimpleChanges) {
    for (let propName in changes) {
      console.log("property Name : " + propName);
      if(propName == "childName") {
        console.log("Property childName found");

        // Get current and previous values
        let change = changes[propName];
        let curVal  = JSON.stringify(change.currentValue);
        let prevVal = JSON.stringify(change.previousValue);
        if(curVal != prevVal) {
          console.log("Property childName has changed.");
        }
      }

    }
  }

  getComponentName() : string {
    return this.childName;
  }
}
